<<<<<<< HEAD
# Requires web server running, serving install files up via HTTP
# Used 'hello-world-nginx' container with the /website_files volume remapped to the project directory for
# this container.   'hello-world-nginx' was built from kinematic/http.
=======
>>>>>>> 1d2a51a8a2639481abd39ce58d386750374a3705

FROM registry.access.redhat.com/rhel7
MAINTAINER Craig Dougan "Craig.Dougan@gmail.com"
ENV hostip=192.168.0.5 \
    webport=32771 
ENV weblink=http://${hostip}:${webport} \
    profilename=DEV1WSPF-TEST \
    servername=DEV1WSMB-TEST1 \
    was_install_file_one=was.repo.8550.developers.ilan_part1.zip \ 
    was_install_file_two=was.repo.8550.developers.ilan_part2.zip \
    was_install_file_three=was.repo.8550.developers.ilan_part3.zip \
    im_install_file=agent.installer.linux.gtk.x86_64_1.8.5000.20160506_1125.zip \
    java_install_file_one=was.repo.8550.java7_part1.zip \
    java_install_file_two=was.repo.8550.java7_part2.zip \
    java_install_file_three=was.repo.8550.java7_part3.zip \
    ansible_key="ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDAiPBCWpyPRVpmGVMA6MiXm/aBQHln/6aKg3hwjER1gmcYIqHom3UXm7DtjogToBNq1nsyfx3IkIFTMxYLci75lMprJ4WlPzLhnY/UOjOf3FfEWN7hQhf1A/ETljk5oZXoLKmNi3dStl0D6l8UjBqodZzI9QfC3fzuk6a20EoSXRZdw32NGD6f8jZaZ0siLeXZTQicdaXW8+/K1E1GVDsOc900Hx/kDn6oLJ2mhEIvuMWjayvESMta+RUMytCVobvvCaYz+eQaXVwHsYu4fFDrfCscbueIryFv5+tNX9y2sXr5jFx1fTOH20ZUyQxNz8bcb5GKDEGyEisygzrNbCcN craig@Craigs-MacBook-Pro.local"

RUN cd /tmp && \
    yum-config-manager --enable rhel-7-server-optional-rpms && \
    yum-config-manager --enable rhel-7-server-extras-rpms && \
    yum-config-manager --enable rhel-7-server-supplementary-rpms && \
    yum install -y wget && \
    yum install -y unzip && \
    yum install -y openssh-server && \
    yum install -y sudo && \
    yum install -y PyYAML && \
    yum install -y xorg-x11-fonts-Type1 && \
    yum install -y xorg-x11-server-common && \
    yum install -y xorg-x11-apps && \
    yum install -y xorg-x11-xkb-utils && \
    yum install -y xorg-x11-server-Xorg && \
    yum install -y xorg-x11-font-utils && \
    yum install -y xorg-x11-xauth && \
    yum --enablerepo rhel-7-server-optional-rpms install -y xorg-x11-server-Xvfb && \
    yum clean all && \
    mkdir /tmp/IM_install && \
    wget $weblink/$im_install_file -O /tmp/$im_install_file && \
    unzip /tmp/$im_install_file -d /tmp/IM_install && \
    /tmp/IM_install/installc -acceptLicense && \
    rm -rf /tmp/IM_install && \
    rm -rf /tmp/${im_install_file}* && \
    mkdir /tmp/was_install && \
    wget $weblink/$was_install_file_one -O /tmp/$was_install_file_one && \
    unzip /tmp/$was_install_file_one -d /tmp/was_install && \
    rm -rf /tmp/${was_install_file_one}* && \
    wget $weblink/$was_install_file_two -O /tmp/$was_install_file_two && \
    unzip /tmp/$was_install_file_two -d /tmp/was_install && \
    rm -rf /tmp/${was_install_file_two}* && \
    wget $weblink/$was_install_file_three -O /tmp/$was_install_file_three && \
    unzip /tmp/$was_install_file_three -d /tmp/was_install && \
    rm -rf /tmp/${was_install_file_three}* && \
    wget $weblink/install_response_file.xml -O /tmp/install_response_file.xml && \
    /opt/IBM/InstallationManager/eclipse/tools/imcl -acceptLicense input /tmp/install_response_file.xml -log /tmp/install_log.xml && \
    rm -rf /tmp/was_install && \
    rm -rf /tmp/install_response_file.xml && \
    mkdir /tmp/JDK7 && \
    wget $weblink/$java_install_file_one -O /tmp/$java_install_file_one && \
    unzip /tmp/$java_install_file_one -d /tmp/JDK7 && \
    rm -rf /tmp/${java_install_file_one}* && \
    wget $weblink/$java_install_file_two -O /tmp/$java_install_file_two && \
    unzip /tmp/$java_install_file_two -d /tmp/JDK7 && \
    rm -rf /tmp/$*{java_install_file_two}* && \
    wget $weblink/$java_install_file_three -O /tmp/$java_install_file_three && \
    unzip /tmp/$java_install_file_three -d /tmp/JDK7 && \
    rm -rf /tmp/${java_install_file_three}* && \
    /opt/IBM/InstallationManager/eclipse/tools/imcl install com.ibm.websphere.IBMJAVA.v70 -repositories /tmp/JDK7 -installationDirectory /opt/IBM/WebSphere/AppServer && \
    rm -rf /tmp/JDK7 && \
    /opt/IBM/WebSphere/AppServer/bin/managesdk.sh -setCommandDefault -sdkname 1.7_64 && \
    /opt/IBM/WebSphere/AppServer/bin/managesdk.sh -setNewProfileDefault -sdkname 1.7_64 && \
    /opt/IBM/WebSphere/AppServer/bin/manageprofiles.sh -create -profileName ${profilename} -serverName ${servername} -templatePath /opt/IBM/WebSphere/AppServer/profileTemplates/default && \
    mkdir -p /usr/local/bin && \
    ssh-keygen -A && \
    echo "#!/bin/bash" > /usr/local/bin/startWAS.sh && \
    echo "sudo -u wasuser /opt/IBM/WebSphere/AppServer/profiles/${profilename}/bin/startServer.sh ${servername}" >> /usr/local/bin/startWAS.sh && \
    echo "/usr/sbin/sshd -D" >> /usr/local/bin/startWAS.sh && \
    chmod u+x /usr/local/bin/startWAS.sh && \
    useradd wasuser && \
    chown -R wasuser:wasuser /opt/IBM/WebSphere && \
    useradd ansible && \
    chage -E -1 -M -1 -d -1 ansible && \
    echo 'ansible ALL=(ALL) NOPASSWD: ALL' > /etc/sudoers.d/ansible && \
    mkdir -p /home/ansible/.ssh && \
    touch /home/ansible/.ssh/authorized_keys && \
    echo "${ansible_key}" > /home/ansible/.ssh/authorized_keys && \ 
    chown -R ansible:ansible /home/ansible/.ssh && \
    chmod 700 /home/ansible/.ssh && \
    chmod 600 /home/ansible/.ssh/authorized_keys && \
    rm -rf /run/nologin

    
# Let's expose at least HTTP and Dmgr(Admin) ports for this profile
EXPOSE 9080 9060 22

# Make sure WAS will be started any time we start docker container with /bin/bash support.

ENTRYPOINT ["/usr/local/bin/startWAS.sh"]
